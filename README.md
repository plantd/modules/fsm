# State Machine Demonstration

This is a trivial example that only demonstrates some functionality, it doesn't
actually do anything useful.

## Controller

Implementation in file `fsm.py`.

## Units

Devices performing interactions in files `unit_{a,b,c}.py`.

## Running

```sh
./build
docker-compose up
```

### Testing

Use `plantcli` from https://gitlab.com/plantd/plantcli.

```sh
./target/plant --config data/config.yaml job fsm -n start
```
