#!/usr/bin/env python

import signal
import sys
import os
import gi
import time

gi.require_version('Apex', '1.0')

#pylint: disable=wrong-import-position

from gi.repository import Apex
from gi.repository import GLib, GObject

from state import constant as c
from state.machine import Machine

#pylint: enable=wrong-import-position

class App(Apex.Application):
    __gtype_name__ = 'App'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, application_id='org.plantd.fsm', flags=0, **kwargs)
        # load environment
        service = os.getenv('PLANTD_MODULE_SERVICE', 'fsm')
        endpoint = os.getenv('PLANTD_MODULE_ENDPOINT', 'tcp://localhost:5555')
        source_endpoint = os.getenv('PLANTD_EVENTS_FRONTEND', 'tcp://localhost:10000')
        # configure application
        self.set_endpoint(endpoint)
        self.set_service(service)
        self.set_inactivity_timeout(10000)
        # messaging devices
        self.event_sink = None
        self.event_source = None
        self.broker_client = Apex.Client.new(endpoint)
        self.machine = Machine(self.broker_client)
        # Initialization
        self.__setup_fsm()
        self.__setup_events(source_endpoint, "")

    def __setup_fsm(self):
        """Setup the state machine using a transition table"""
        for transition in self.machine.transitions.values():
            transition.connect("event", self.on_publish_event)

    def __setup_events(self, endpoint, msg_filter):
        """Setup the event bus"""
        self.event_source = Apex.Source.new(endpoint, msg_filter)
        self.add_source("event", self.event_source)
        self.event_sink = EventSink(self.machine.fsm)

    def do_activate(self):
        Apex.debug('performing activation')
        self.event_sink.start()
        self.event_source.start()
        self.machine.fsm.start()

    def do_shutdown(self):
        Apex.debug('performing shutdown')
        self.event_sink.stop()
        self.event_source.stop()
        self.machine.fsm.stop()

    def on_publish_event(self, user_data, event):
        """Send an event to the connected bus sink for event messages"""
        Apex.debug('fsm sending event: %s' % event.serialize())
        try:
            self.send_event(event)
        except GLib.Error as err:
            if err.matches(Apex.apex_error_quark(),
                           Apex.ApexErrorEnum.NOT_CONNECTED):
                Apex.error('Event source not connected')
            else:
                raise

    def do_submit_job(self, job_name, job_value, job_properties):
        Apex.debug('fsm submit-job: %s [%s]' % (job_name, job_value))
        response = Apex.JobResponse.new()
        if job_name == 'start':
            job = StartJob()
            job.connect('event', self.on_publish_event)
            response.set_job(job)
        else:
            pass
        return response


class StartJob(Apex.Job):
    __gtype_name__ = 'StartJob'
    __gsignals__ = {
        'event': (GObject.SignalFlags.RUN_LAST, object, (object,)),
    }

    def do_task(self):
        Apex.debug('fsm start job')
        event = Apex.Event.new_full(c.E_START, 'start', 'fsm start')
        self.emit('event', event)


class EventSink(Apex.Sink):
    __gtype_name__ = 'EventSink'

    def __init__(self, fsm, *args, **kwargs):
        endpoint = os.getenv('PLANTD_EVENTS_BACKEND', 'tcp://localhost:10001')
        super().__init__(*args, **kwargs)
        Apex.debug(f'connecting sink to endpoint: {endpoint}')
        self.fsm = fsm
        self.set_endpoint(endpoint)
        self.set_filter('')

    def do_handle_message(self, msg):
        Apex.debug(f'sink received: {msg}')
        event = Apex.Event.new()
        event.deserialize(msg)
        self.fsm.submit_event(event)


def run_module():
    log_file = os.getenv('PLANTD_MODULE_LOG_FILE', '/dev/null')

    Apex.log_init(True, log_file)

    app = App()
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    status = app.run(sys.argv)

    Apex.log_shutdown()

    return status

def main():
    return run_module()

if __name__ == '__main__':
    sys.exit(main())
