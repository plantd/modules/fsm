"""Constants for state machine setup and execution.

States represent a position in a state machine, transitions represent a
movement in the transition table, and events trigger a check of the
state machine transition table. Each transition contains a start and end
state, as well as condition for execution, and execution callback.
"""

#
# States
#

S_IDLE = 1000
S_UNIT_A = 1001
S_UNIT_B = 1002
S_UNIT_C = 1003

#
# Transitions
#

T_IDLE_TO_UNIT_A = 2000
T_UNIT_A_TO_UNIT_B = 2001
T_UNIT_B_TO_UNIT_C = 2002
T_UNIT_C_TO_IDLE = 2003

#
# Events
#

E_START = 1000
E_STARTED = 1001
E_FINISHED = 1002

E_UNIT_A_STARTED = 2000
E_UNIT_A_RUNNING = 2001
E_UNIT_A_FINISHED = 2002

E_UNIT_B_STARTED = 3000
E_UNIT_B_RUNNING = 3001
E_UNIT_B_FINISHED = 3002

E_UNIT_C_STARTED = 4000
E_UNIT_C_RUNNING = 4001
E_UNIT_C_FINISHED = 4002
