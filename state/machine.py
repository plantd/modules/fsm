import os
import configparser
import gi

gi.require_version('Apex', '1.0')

#pylint: disable=wrong-import-position

from gi.repository import Apex, GLib, GObject

from . import constant as c
from .transition import Transition

#pylint: enable=wrong-import-position

class Machine:
    def __init__(self, broker):
        """Construction"""
        self.fsm = Apex.StateMachine.new()
        self.table = Apex.TransitionTable.new()
        self.broker = broker
        # state setup
        self.states = {}
        self.s_defs = {
            c.S_IDLE: {"name": "idle", "desc": "Idle state"},
            c.S_UNIT_A: {"name": "unit_a", "desc": "Unit a state"},
            c.S_UNIT_B: {"name": "unit_b", "desc": "Unit b state"},
            c.S_UNIT_C: {"name": "unit_c", "desc": "Unit c state"},
        }
        self.__init_states()
        # transition setup
        self.transitions = {}
        self.t_defs = {
            c.T_IDLE_TO_UNIT_A: {
                "type": GObject.type_from_name("IdleToUnitA"),
                "start": self.states[c.S_IDLE],
                "end": self.states[c.S_UNIT_A],
            },
            c.T_UNIT_A_TO_UNIT_B: {
                "type": GObject.type_from_name("UnitAToUnitB"),
                "start": self.states[c.S_UNIT_A],
                "end": self.states[c.S_UNIT_B],
            },
            c.T_UNIT_B_TO_UNIT_C: {
                "type": GObject.type_from_name("UnitBToUnitC"),
                "start": self.states[c.S_UNIT_B],
                "end": self.states[c.S_UNIT_C],
            },
            c.T_UNIT_C_TO_IDLE: {
                "type": GObject.type_from_name("UnitCToIdle"),
                "start": self.states[c.S_UNIT_C],
                "end": self.states[c.S_IDLE],
            },
        }
        self.__init_transitions()
        self.__init_table()
        self.fsm.connect("state-changed", self.on_state_changed)

    def __init_states(self):
        for k, v in self.s_defs.items():
            Apex.debug("create state %s(%d): %s" % (v.get("name"), k,
                                                    v.get("desc")))
            self.states[k] = Apex.State.new_full(k, v.get("name"), v.get("desc"))

    def __init_transitions(self):
        for k, v in self.t_defs.items():
            Apex.debug("create transition %s(%d): %s -> %s" % (v.get("type"), k,
                                                               v.get("start").get_name(),
                                                               v.get("end").get_name()))
            self.transitions[k] = GObject.new(v.get("type"), **{"broker": self.broker})
            self.transitions[k].set_id(k)
            self.transitions[k].set_start_state(v.get("start"))
            self.transitions[k].set_end_state(v.get("end"))

    def __init_table(self):
        Apex.debug("add transition table to fsm")
        for v in self.transitions.values():
            self.table.add(v)
        self.fsm.set_transition_table(self.table)

    def on_state_changed(self, *args):
        state = args[1]
        Apex.info("state was changed to %s (%d): %s" % (state.get_name(),
                                                        state.get_id(),
                                                        state.get_description()))


class IdleToUnitA(Transition):
    """IdleToUnitA"""
    __gtype_name__ = "IdleToUnitA"

    __gsignals__ = {
        'event': (GObject.SIGNAL_RUN_LAST, object, (object,))
    }

    def do_evaluate(self, event):
        if event.get_id() == c.E_START:
            return True
        return False

    def do_execute(self, data):
        req = Apex.ModuleJobRequest.new()
        req.set_id('unit_a')
        req.set_job_id('start')
        req.set_job_value('unit-a start')
        req.add(Apex.Property.new('count', '5'))
        msg = req.serialize()
        Apex.debug(f'send message: {msg}')
        self.get_property("broker").send_request('unit_a', 'submit-job', msg)
        resp = self.get_property("broker").recv_response()
        if resp is None:
            Apex.warning('exec: failed to submit start to unit a')


class UnitAToUnitB(Transition):
    """UnitAToUnitB"""
    __gtype_name__ = "UnitAToUnitB"

    __gsignals__ = {
        'event': (GObject.SIGNAL_RUN_LAST, object, (object,))
    }

    def do_evaluate(self, event):
        if event.get_id() == c.E_UNIT_A_FINISHED:
            return True
        return False

    def do_execute(self, data):
        req = Apex.ModuleJobRequest.new()
        req.set_id('unit_b')
        req.set_job_id('start')
        req.set_job_value('unit-b start')
        req.add(Apex.Property.new('count', '5'))
        msg = req.serialize()
        Apex.debug(f'send message: {msg}')
        self.get_property("broker").send_request('unit_b', 'submit-job', msg)
        resp = self.get_property("broker").recv_response()
        if resp is None:
            Apex.warning('exec: failed to submit start to unit b')


class UnitBToUnitC(Transition):
    """UnitBToUnitC"""
    __gtype_name__ = "UnitBToUnitC"

    __gsignals__ = {
        'event': (GObject.SIGNAL_RUN_LAST, object, (object,))
    }

    def do_evaluate(self, event):
        if event.get_id() == c.E_UNIT_B_FINISHED:
            return True
        return False

    def do_execute(self, data):
        req = Apex.ModuleJobRequest.new()
        req.set_id('unit_c')
        req.set_job_id('start')
        req.set_job_value('unit-c start')
        req.add(Apex.Property.new('count', '5'))
        msg = req.serialize()
        Apex.debug(f'send message: {msg}')
        self.get_property("broker").send_request('unit_c', 'submit-job', msg)
        resp = self.get_property("broker").recv_response()
        if resp is None:
            Apex.warning('exec: failed to submit start to unit c')


class UnitCToIdle(Transition):
    """UnitCToIdle"""
    __gtype_name__ = "UnitCToIdle"

    __gsignals__ = {
        'event': (GObject.SIGNAL_RUN_LAST, object, (object,))
    }

    def do_evaluate(self, event):
        if event.get_id() == c.E_UNIT_C_FINISHED:
            return True
        return False

    def do_execute(self, data):
        pass
