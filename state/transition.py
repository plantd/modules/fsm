import gi

gi.require_version('Apex', '1.0')

#pylint: disable=wrong-import-position

from gi.repository import Apex, GObject

#pylint: enable=wrong-import-position

class Transition(Apex.Transition):
    """Transition"""
    __gtype_name__ = "Transition"

    __broker = None

    @GObject.Property(type=Apex.Client)
    def broker(self):
        return self.__broker

    @broker.setter
    def broker(self, value):
        self.__broker = value
