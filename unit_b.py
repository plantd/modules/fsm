#!/usr/bin/env python

import signal
import sys
import time
import os
import gi

gi.require_version('Apex', '1.0')

#pylint: disable=wrong-import-position

from gi.repository import Apex
from gi.repository import GLib, GObject

from state import constant as c

#pylint: enable=wrong-import-position

class App(Apex.Application):
    __gtype_name__ = 'App'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, application_id='org.plantd.unit_b', flags=0, **kwargs)
        # load environment
        service = os.getenv('PLANTD_MODULE_SERVICE', 'unit_b')
        endpoint = os.getenv('PLANTD_MODULE_ENDPOINT', 'tcp://localhost:5555')
        source_endpoint = os.getenv('PLANTD_EVENTS_FRONTEND', 'tcp://localhost:10000')
        # configure application
        self.set_endpoint(endpoint)
        self.set_service(service)
        self.set_inactivity_timeout(10000)
        # setup events
        self.event_source = Apex.Source.new(source_endpoint, '')
        self.add_source('event', self.event_source)
        self.event_source.start()

    def on_publish_event(self, user_data, event):
        """Send an event to the connected bus sink for event messages"""
        Apex.debug('sending event: %s' % event.serialize())
        try:
            self.send_event(event)
        except GLib.Error as err:
            if err.matches(Apex.apex_error_quark(),
                           Apex.ApexErrorEnum.NOT_CONNECTED):
                Apex.error('Event source not connected')
            else:
                raise

    def do_submit_job(self, job_name, job_value, job_properties):
        """Handle a job that was submitted"""
        Apex.debug('submit-job: %s [%s]' % (job_name, job_value))
        response = Apex.JobResponse.new()
        if job_name == 'start':
            job = StartJob()
            job.connect('event', self.on_publish_event)
            response.set_job(job)
        else:
            pass
        return response


class StartJob(Apex.Job):
    __gtype_name__ = 'StartJob'
    __gsignals__ = {
        'event': (GObject.SignalFlags.RUN_LAST, object, (object,)),
    }

    def do_task(self):
        Apex.debug('unit b start job')
        event = Apex.Event.new_full(c.E_UNIT_B_STARTED, 'started', 'unit b started')
        self.emit('event', event)
        time.sleep(.5)
        n = 0
        while n < 5:
            event = Apex.Event.new_full(c.E_UNIT_B_RUNNING, 'running', 'unit b running')
            self.emit('event', event)
            time.sleep(1)
            n = n + 1
        event = Apex.Event.new_full(c.E_UNIT_B_FINISHED, 'finished', 'unit b finished')
        self.emit('event', event)


def run_module():
    log_file = os.getenv('PLANTD_MODULE_LOG_FILE', '/dev/null')

    Apex.log_init(True, log_file)

    app = App()
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    status = app.run(sys.argv)

    Apex.log_shutdown()

    return status

def main():
    return run_module()

if __name__ == '__main__':
    sys.exit(main())
